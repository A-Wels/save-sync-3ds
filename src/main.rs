use ftp::FtpStream;
mod structs;
use structs::{Device, Game, GameType, GBA_SAVE_TYPES};
use std::fs::{self, File};
use std::io::Write;


const IP: &str = "192.168.178.49";
const PORT: u16 = 5000;

fn main() {
    let save_path_server = "/home/alex/.config/retroarch/saves";
    let state_path_server = "/home/alex/.config/retroarch/states";

    let ds3 = Device {
        name: "DS3".to_string(),
        address: "192.168.178.49:5000".to_string(),
        games: get_games(),
    };


    let devices = vec![ds3];

    for device in devices {
        // Check if device is available
        println!("Connecting to: {} at {}", device.name, device.address);
        let ftp_stream = FtpStream::connect(device.address);
        if let Err(e) = ftp_stream {
            println!("Device unavailable: {}\n", e);
            continue;
        }

        // Login to device
        println!("Connected to: {}\n", device.name);
        let mut ftp_stream = ftp_stream.unwrap();
        ftp_stream.login("anonymous", "anonymous").unwrap();

        // Get savegames for each game associated with device
        for game in device.games {
            println!("Getting savegames for: {} in {}", game.name, game.save_path);
            let save_path: Option<&str> = Some(game.save_path.as_str());
            let files = ftp_stream.nlst(save_path).unwrap();
            let files = filter_savegames(files, game.game_type);
            println!("Files: {:?}", files);

            // Create savegame folder if it doesn't exist
            println!("Creating savegame folder: {}", save_path_server);
            let result = fs::create_dir_all(save_path_server);
            if let Err(e) = result {
                println!("Failed to create savegame folder: {}\n", e);
                continue;
            }


            // Get savegame and save it locally
            for filepath in files {
                println!("Grabbing file: {}", filepath);
                // get filename: split at last '/' and take second part
                let file_name = filepath.split('/').last().unwrap();

                let savegame = ftp_stream.simple_retr(&filepath);
                if let Err(e) = savegame {
                    println!("Failed to grab file: {}\n", e);
                    continue;
                }
                let mut savegame = savegame.unwrap();

                // write to disk
                let mut local_file_path = format!("{}/{}", save_path_server, file_name);
                // replace .sav with .srm
                local_file_path = local_file_path.replace(".sav", ".srm");
                println!("Saving to: {}", local_file_path);
                let mut local_file = std::fs::File::create(local_file_path).unwrap();
                std::io::copy(&mut savegame, &mut local_file).unwrap();
            }

        let _ = ftp_stream.quit();
        }
    }
}

fn get_games() -> Vec<Game> {
    let mut games = Vec::new();

    let emerald = Game {
        name: "Pokemon - Emerald Version (USA, Europe).gba".to_string(),
        game_type: GameType::GBA,
        path: "/_gba".to_string(),
        save_path: "/_gba".to_string(),
        state_path: "/_gba".to_string(),
    };
    games.push(emerald);
    games
}

fn filter_savegames(files: Vec<String>, gameType: GameType) -> Vec<String> {
    let mut savegames = Vec::new();

    for file in files {
        let filename = file.clone();
        match gameType {
            GameType::GBA => {
                for file_type in GBA_SAVE_TYPES {
                    if file.ends_with(file_type) {
                        savegames.push(filename);
                        break;
                    }
                }
            }
        }
    }
    savegames
}
