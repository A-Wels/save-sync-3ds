pub enum GameType{
    GBA
}

pub static GBA_SAVE_TYPES: [&'static str; 1] = [".sav"];
pub static GBA_STATE_TYPES: [&'static str; 1] = [".ss"];
pub struct Game {
    pub  name: String,
    pub path: String,
    pub game_type: GameType,
    pub save_path: String,
    pub state_path: String,
}

pub struct Device {
    pub  name: String,
    pub address: String,
    pub games: Vec<Game>,
}
